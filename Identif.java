package ru.peo.identifier;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Identif {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("([A-Za-z_])(\\w)*"); //w-	Любая буква или цифра, * - совпадает ноль или более раз
        /**Класс Pattern(шаблон) представляет собой скомпилированное представление РВ. Класс не имеет публичных конструкторов,
        *поэтому для создания
        *\объекта данного класса необходимо вызвать статический метод compile и передать в качестве первого аргумента строку с РВ
        try{
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\User\\Desktop\\Учеба\\2 курс и 1курс\\ОП\\2курс2симестрОП\\ОП2_15ОИТ20_Пылайкина.Е\\Идентификаторы\\indecators\\src\\ru\\peo\\Indef\\Jins.java"));
            BufferedWriter bw = new BufferedWriter(new FileWriter("TXT.txt"));
            Matcher math ; //Matcher — класс, который представляет строку, реализует механизм согласования (matching)
            // с РВ и хранит результаты этого согласования (используя реализацию методов интерфейса MatchResult).
            // Не имеет публичных конструкторов, поэтому для создания объекта этого класса нужно использовать метод matcher класса Pattern
            String check;
            String write;
            while((check=br.readLine())!=null){//пока не равно нулю читаем строку
                math = pattern.matcher(check);//проверка на совпадения по шаблону
                if (math.find()){//если найдем совпадения
                    System.out.println(math.group());
                    bw.write(math.group()+"\n");
                }
            }
            br.close();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}